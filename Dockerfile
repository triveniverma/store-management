FROM python:3.7

ENV PYTHONUNBUFFERED 1

RUN apt-get update -y && \
    apt-get install -y python-pip python-dev && \
    pip3 install pipenv 


# We copy just the requirements first to leverage Docker cache
COPY ./Pipfile.lock /app/Pipfile.lock

COPY ./Pipfile /app/Pipfile


WORKDIR /app

COPY ./store_management /app

RUN ls -l

EXPOSE 8000

RUN pipenv install --system --ignore-pipfile

#CMD ["python", "manage.py", "runserver"]
