'''
    python imports
'''
import os
'''
    third party imports
'''
import stripe
stripe.api_key = os.getenv("API_KEY")
from celery import current_app
from rest_framework import generics
from rest_framework import filters
from rest_framework.permissions import IsAuthenticated 
from rest_framework.views import APIView 
from rest_framework.response import Response
from rest_framework import status
'''
    django imports
'''
import json
from django.shortcuts import render
from django.http import JsonResponse
from django.core import serializers
from .serializers import CustomerSerializer, ProductSerializer, CartSerializer, ProductCartSerializer, ChangePasswordSerializer
from django.http import Http404
from django.core.mail import send_mail
from django.conf import settings
from django.contrib.auth.models import User

# local imports
from product.models import Product
from coupons.models import Coupon
from customer.models import CustomerRecord, Cart, ProductCart, Transaction, Orders
from .tasks import send_email
from utils.controllers import stripe_controller
# Create your views here.


class CreateCustomer(generics.ListCreateAPIView):
    '''
        creating customer through api,
        encrypting the password and 
        generating a stripe_id
    '''
    queryset = CustomerRecord.objects.all()
    serializer_class = CustomerSerializer

    def perform_create(self, serializer):
        instance = serializer.save()
        instance.set_password(instance.password)
        stripe_customer = stripe_controller.create_customer(serializer.data["username"])
        instance.stripe_id = stripe_customer.id
        instance.save()

class ChangePasswordView(generics.UpdateAPIView):
    """
    An endpoint for changing password.
    """
    serializer_class = ChangePasswordSerializer
    model = User
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'Password updated successfully',
            }

            return Response(response)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ProductList(generics.ListCreateAPIView):
    '''
        displaying product list through api
    '''
    # def get_response():
    #     message = {'message': 'No products present'}
    #     return Response(message, status=status.HTTP_400_BAD_REQUEST)
    # try:
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_backends = [filters.OrderingFilter, filters.SearchFilter]
    ordering_fields = ['Price', 'Category', 'Quantity']
    search_fields = ['Category']

    # def list(self, request):
    #     # Note the use of `get_queryset()` instead of `self.queryset`
    #     queryset = self.get_queryset()
    #     serializer = ProductSerializer(queryset, many=True)
    #     return Response(serializer.data)

class CartView(APIView):
    '''
        to add items to the cart
    '''
    permission_classes = (IsAuthenticated, )

    def get_cart(self, pk):
        '''
            function will check if cart exists else will create the cart
        '''
        if Cart.objects.filter(customer_id=pk).exists():
            return Cart.objects.get(customer_id=pk)
        else:
            customer_instance = CustomerRecord.objects.get(id=pk)
            create_cart = Cart(customer_id=customer_instance)
            create_cart.save()
            return Cart.objects.get(customer_id=pk)

    def get_amount(self, product_id, quantity):
        product = Product.objects.get(id=product_id)
        total_price = product.Price * int(quantity)
        return total_price

    def post(self, request):
        '''
            function will add product to the cart, 
            and if same product is added then quantity is updated
        '''
        cart_id = request.user.id
        getcart_id = self.get_cart(cart_id)
        check_product = ProductCart.objects.filter(cart_id=getcart_id, product_id=request.data["product_id"])
        product_exists = check_product.exists()
        if product_exists:
                total_quantity = int(request.data["quantity"]) + int(check_product[0].quantity)
                calculate_amount = self.get_amount(request.data["product_id"], total_quantity)
                check_product.update(quantity=total_quantity, amount=calculate_amount)
                data = {'cart_id':getcart_id, 
                         'product_id':request.data["product_id"],
                         'quantity':check_product[0].quantity
                        }
                return Response(status=status.HTTP_200_OK)
            
        else:
            calculate_amount = self.get_amount(request.data["product_id"], request.data["quantity"])
            serializer = ProductCartSerializer(request.data.update({'cart_id':getcart_id.id,'amount':calculate_amount}), request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors)

    def get(self, request):
        '''
            function will give the list of product in one's cart
        '''
        cart_id = request.user.id
        getcart_id = self.get_cart(cart_id)
        product_cart = ProductCart.objects.filter(cart_id=getcart_id)
        serializer = ProductCartSerializer(product_cart, many=True)
        return Response(serializer.data)


class CartUpdateView(APIView):
    '''
        class to make changes in existing cart
    '''
    
    def get_object_productcart(self, pk):
        '''
            function will give the cart or 
            will give the not found response
        '''
        try:
            return ProductCart.objects.get(pk=pk)
        except ProductCart.DoesNotExist:
            raise Http404()

    def get(self, request, pk):
        '''
            function will give the data in response 
            in respective to cart_id
        '''
        product_cart = self.get_object_productcart(pk)
        serializer = ProductCartSerializer(product_cart)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, pk):
        '''
            function will let you change the data in cart 
            in respective to cart_id
        '''
        product_cart = self.get_object_productcart(pk)
        serializer = ProductCartSerializer(product_cart, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    #def put(self, request, pk):
    #     product_cart = self.get_object_productcart(pk)
    #     # import pdb; pdb.set_trace()
    #     # if(product_cart.quantity == request.data['quantity']):
    #     serializer = ProductCartSerializer(product_cart, data=request.data)
    #     if serializer.is_valid():
    #         serializer.save()
    #         return Response(serializer.data, status=status.HTTP_200_OK)
    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)        


    def delete(self, request, pk):
        '''
            function will delete the cart
        '''
        product_cart = self.get_object_productcart(pk)
        product_cart.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)




class PlaceOrder(APIView):
    '''
        class to place the order
    '''
    def amount_calc(self, cart_id):
        # import pdb; pdb.set_trace()
        final_amount = 0
        product_carts = ProductCart.objects.filter(cart_id=cart_id)
        for product in product_carts:
            final_amount += product.amount
        update_cart = Cart.objects.filter(id=cart_id)
        update_cart.update(total_amount=final_amount, status='active')
        return final_amount

    def coupon_calc(self, coupon_code, before_amount):
        get_coupon = Coupon.objects.filter(code=coupon_code)
        if get_coupon.exists():
            after_amount = 0
            if get_coupon[0].coupon_description == "IN AMOUNT":
                coupon_val = get_coupon[0].coupon_value
                after_amount = before_amount - coupon_val
            else:
                coupon_val = get_coupon[0].coupon_value
                after_amount = before_amount - before_amount * (coupon_val/100)
            return after_amount


    def get_customer_card(self, cart_id):
        '''
            function will check if the customer has 
            some active card(CC) or not
        '''
        try:
            # import pdb; pdb.set_trace()
            cart_customer = Cart.objects.get(id=cart_id)
            customer_record = CustomerRecord.objects.get(username=cart_customer)
            # stripe_record = stripe.Customer.retrieve(customer_record.stripe_id)
            stripe_record = stripe_controller.retrieve_record(customer_record.stripe_id)
            if(stripe_record.default_source != None):
                return customer_record.stripe_id
            else:
               return None
        except Cart.DoesNotExist:
            message = {'message': "Cart Does Not Exist"}
            return Response(message, status=status.HTTP_404_NOT_FOUND)
        except CustomerRecord.DoesNotExist:
            raise Http404()

    def update_product_quantity(self, cart_id):
        all_products = ProductCart.objects.filter(cart_id=cart_id)
        for items in all_products:
            find_item = Product.objects.get(id=items.product_id.id)
            find_item.Quantity -=  items.quantity
            find_item.save()



    def post(self, request, pk):
        '''
            If active card is not present then
            it will give the response accordingly
            else will let you make the payment
        '''
        get_amount = self.amount_calc(pk)
 
        get_card = self.get_customer_card(pk)
        if get_card is None:
            content = {'message':"No active card found"}
            return Response(content, status=status.HTTP_200_OK)
        else:
            update_cart = Cart.objects.filter(id=pk)
            if request.data:
                coupon = request.data["coupon_code"]
                update_coupon = Coupon.objects.get(code=coupon)
                after_coupon_value = self.coupon_calc(coupon, get_amount)
                get_amount = after_coupon_value
                update_cart.update(discount_amount=get_amount, coupon_code=update_coupon)
            else:
                update_cart.update(discount_amount=get_amount, coupon_code=None)
            
            charge_generated = stripe_controller.create_charge("Final Order",
                                                                get_card,
                                                                "inr",
                                                                int(get_amount) *(100)
                                                               )

            if(charge_generated.outcome["network_status"] and charge_generated.outcome["type"]):
                customer_record = CustomerRecord.objects.get(stripe_id=get_card)
                create_transaction = Transaction(stripe_id=customer_record,
                                                 status='Payment Done',
                                                 total_amount=get_amount
                                                )
                create_transaction.save()

                name = customer_record
                send_email.delay(customer_record.username, customer_record.email)
                # print(f"id={task.id}, state={task.state}, status={task.status}")
                
                update_quantity = self.update_product_quantity(pk)
                # create_order = Orders()

                content = {'message': charge_generated.outcome["seller_message"] }
                return Response(content, status=status.HTTP_200_OK)
            else:
                content = {'message': charge_generated.outcome["seller_message"] }
                return Response(content, status=status.HTTP_402_PAYMENT_REQUIRED)

class CreateCard(APIView):
    '''
        to generate test card details 
    '''
    def post(self, request, pk):
        '''
            corresponding to the given customer a card is
            generated to make payment
        '''
        customer_record = CustomerRecord.objects.get(pk=pk)
        stripe_controller.create_source(customer_record.stripe_id)

        content = {'message': 'Test card created'}
        return Response(content, status=status.HTTP_200_OK)

