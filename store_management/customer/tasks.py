from django.core.mail import send_mail
from django.conf import settings
from celery import shared_task

@shared_task
def send_email(user, user_email):
    subject = "Payment Successful"
    message = f'Hello {user} you have successfully made the payment'
    recipient_list = [user_email]
    email_from = settings.EMAIL_HOST_USER
    send_mail(subject, message, email_from, recipient_list)

