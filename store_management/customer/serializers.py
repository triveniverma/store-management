'''
    serializer file for customer
'''
# third party imports
from rest_framework import serializers
# django imports
from django.contrib.auth.models import User

# local imports
from customer.models import CustomerRecord, Cart, ProductCart
from product.models import Product


class CustomerSerializer(serializers.ModelSerializer):
    '''
        creating a serializer for customer
    '''
    class Meta:
        '''
            providing information about outer class
        '''
        model = CustomerRecord
        exclude = ['created_at']


class ProductSerializer(serializers.ModelSerializer):
    '''
        creating a serializer for product
    '''
    class Meta:
        '''
            providing information about outer class
        '''
        model = Product
        fields = '__all__'
        
    
 
class CartSerializer(serializers.ModelSerializer):
    '''
        creating a serializer for cart
    '''
    class Meta:
        '''
            providing information about outer class
        '''
        model = Cart
        fields = '__all__'

class ProductCartSerializer(serializers.ModelSerializer):
    '''
        creating a serializer for product cart
    '''
    class Meta:
        '''
            providing information about outer class
        '''
        model = ProductCart
        fields = '__all__'

class ChangePasswordSerializer(serializers.Serializer):
    model = User

    """
    Serializer for password change endpoint.
    """
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)