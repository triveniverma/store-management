from django.contrib import admin
from .models import CustomerRecord, Cart, ProductCart, Transaction

admin.site.register(CustomerRecord)
admin.site.register(Cart)
admin.site.register(ProductCart)
admin.site.register(Transaction)