'''
    django imports
'''
from django.urls import path, include
# django imports
from customer import views


urlpatterns = [
    path('customer/', views.CreateCustomer.as_view(), name='create_customer'),
    path('products/', views.ProductList.as_view(), name='products'),
    path('cart/', views.CartView.as_view(), name ='cart'),
    path('cart/<int:pk>', views.CartUpdateView.as_view(), name ='updatecart'),
    path('order/<int:pk>', views.PlaceOrder.as_view(), name='place_order'),
    path('create_card/<int:pk>', views.CreateCard.as_view(), name='card'),
    path('create_card/<int:pk>', views.CreateCard.as_view(), name='card'),
    path('api/password_reset/', include('django_rest_passwordreset.urls', namespace='password_reset')),
    path('api/change-password/', views.ChangePasswordView.as_view(), name='change-password'),
]
