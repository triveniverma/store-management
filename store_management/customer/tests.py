from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework.test import APITestCase
from customer.models import CustomerRecord, Cart, ProductCart
from product.models import Product
from coupons.models import Coupon
from customer.serializers import CustomerSerializer, ProductCartSerializer
from django.contrib.auth.models import User
from rest_framework.test import force_authenticate
from mock import patch
import requests
from utils.controllers import stripe_controller
from customer.tasks import send_email

def mock_create_customer(self):
    print("mocked")
    class StripeMocked:
        def __init__(self):
            self.id = "cust_12345"
    return StripeMocked()

class CustomerTesting(APITestCase):

    @patch('utils.controllers.stripe_controller.create_customer', side_effect=mock_create_customer)
    def test_customer(self, *args, **kwargs):
        valid_data = {
                        "username":"abcd",
                        "password":"123456",
                        "first_name":"new",
                        "last_name":"new",
                        "email":"new123@gmail.com",
                        "mobileno":"8900911134",
                        "address":"xjxjsjsj",
                        "city":"indore",
                     }

        invalid_data = {
                            "username":"abcd",
                            "password":"123456",
                            "first_name":"new",
                            "last_name":"new",
                            "email":"new123@gmail.com",
                            "mobileno":"8900911134",
                            "address":"xjxjsjsj",
                            "city":"indore",
                        }

        response = self.client.post('/customer/customer/', valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client.post('/customer/customer/', valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

def mock_create_charge(self, *args):
    print("mocked charge")
    class StripeMocked:
        def __init__(self):
            self.outcome = {}
            self.outcome['network_status'] = "Active"
            self.outcome['type'] = "Default"
            self.outcome['seller_message'] = "Payment Done"
    return StripeMocked()

def mock_retrieve_record(self):
    print("mocked retrieve record")
    class StripeMocked:
        def __init__(self):
            self.default_source = "cust_12345"
    return StripeMocked()

def mock_customer_card(self):
    print("demo card creation")

class CustomerLogin(APITestCase):

    def setUp(self):

        self.customer = CustomerRecord.objects.create_user(username="abcd123",
        password="123456",
        first_name="new",
        last_name="new",
        email="new123@gmail.com",
        mobileno="890009134",
        address="xjxjsjsj",
        city="indore"
        )

        self.username = 'abcd123'
        self.password = '123456'

        info = {
        'username': self.username,
        'password': self.password
        }
        response = self.client.post('/api/token/', info, format='json')
        self.token = response.data['access']
        
        # info_other = {
        # 'username': 'abcde123',
        # 'password': '123456'
        # }

        # response = self.client.post('/api/token/', info_other, format='json')
        # self.token_other = response.data['access']

        self.product = Product.objects.create(Name='dish washer',
        Category='daily',
        Price=50,
        Description='hhh',
        Quantity=10,
        Created_By='aisha'
        )

        Product.objects.create(Name='dish',
        Category='daily',
        Price=30,
        Description='hhh',
        Quantity=10,
        Created_By='aisha'
        )

        Cart.objects.create(
        customer_id=CustomerRecord.objects.get(id=1)
        )

        ProductCart.objects.create(
        product_id=Product.objects.get(id=1),
        cart_id=Cart.objects.get(id=1),
        quantity=2,
        )

        Coupon.objects.create(
            code='1500',
            coupon_value=200,
            start_date='2020-10-30',
            expire_date='2020-12-8'
        )
        Coupon.objects.create(
            code='200ABC',
            coupon_value=20,
            coupon_description='IN PERCENTAGE',
            start_date='2020-10-30',
            expire_date='2020-12-8'
        )


    def test_login_api(self):

        self.username = 'abcd123'
        self.password = '123456'
        valid_data = {
                    'username': self.username,
                    'password': self.password
        }

        self.username = 'abcd1'
        self.password = '123456'
        invalid_data = {
                    'username': self.username,
                    'password': self.password
        }

        self.username = 'abcd1'
        self.password = ''
        invalid_data_pwd = {
                    'username': self.username,
                    'password': self.password
        }


        response = self.client.post('/api/token/', valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)
        response = self.client.post('/api/token/', invalid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        response = self.client.post('/api/token/', invalid_data_pwd, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


    def test_add_product(self):
        valid_data = {
            "product_id":"2",
            "quantity":"4"
        }
        invalid_data = {
            "product_id":"2",
            "quantity": "4"
        }
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)
        response = client.post('/customer/cart/', valid_data, format='json')
        print("res",response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = client.post('/customer/cart/', valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_products(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)
        response = client.get('/customer/products/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_products_ordering(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)
        response = client.get('/customer/products/?ordering=-Price')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_products_filtering(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)
        response = client.get('/customer/products/?Category=daily')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_customer_cartwise(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)
        response = client.get('/customer/cart/')
        print("res12", response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_product_cartwise(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)
        response = client.get('/customer/cart/1')
        print("res12", response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = client.get('/customer/cart/4')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_patch_product_cartwise(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)
        response = client.patch('/customer/cart/1', {'quantity':6})
        print("res12", response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = client.patch('/customer/cart/4', {'quantity':6})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_product_cartwise(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)
        response = client.delete('/customer/cart/1')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = client.delete('/customer/cart/7')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    @patch('utils.controllers.stripe_controller.create_charge', side_effect=mock_create_charge)
    @patch('utils.controllers.stripe_controller.retrieve_record', side_effect=mock_retrieve_record)
    def test_order_cart(self, *args, **kwargs):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)
        data = {
            "coupon_code" : "1500"
        }
        percent_data = {
            "coupon_code" : "200ABC"
        } 
        response = client.post('/customer/order/1', format='json')
        print("hello", response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = client.post('/customer/order/1', data, format='json')
        print("hello", response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = client.post('/customer/order/1', percent_data, format='json')
        print("hello", response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        
    # @patch('utils.controllers.stripe_controller.create_charge', side_effect=mock_create_charge)
    # @patch('utils.controllers.stripe_controller.retrieve_record', side_effect=mock_retrieve_record)
    # def test_order_cart_other(self, *args, **kwargs):
    #     client = APIClient()
    #     client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token_other)
    #     response = client.post('/customer/order/2', format='json')
    #     print("hello", response.data)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)

    @patch('utils.controllers.stripe_controller.create_source', side_effect=mock_customer_card)
    def test_create_card(self, *args, **kwargs):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)
        response = client.post('/customer/create_card/1', format='json')
        print(response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_change_password(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)
        data = {
                    "old_password": "123456",
                    "new_password": "123456"
               }
        password_wrong = {

                    "old_password": "12345",
                    "new_password": "123456"
                }
        invalid_data = {

                    "old_password": " ",
                    "new_password": "123456"
        }
        response = client.put('/customer/api/change-password/', data, format='json')
        print("password changed", response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = client.put('/customer/api/change-password/', password_wrong, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        response = client.put('/customer/api/change-password/', invalid_data, format='json')
        print("password changed", response.data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_reset_api_token(self):
        client = APIClient()
        data = {
                    "email": "new123@gmail.com"
        }
        response = client.post('/customer/api/password_reset/', data, format='json')
        print("password reset token", response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_reset_api_confirm(self):
        client = APIClient()
        data = {
                     "token":"e8d6929eb0db62aa0dfd2676d1e4a790c",
                     "password":"Password@123"
        }
        response = client.post('/customer/api/password_reset/confirm/', data, format='json')
        print("password reset token", response.data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_noerror(self):
        result = send_email.delay("cus123", "new123@gmail.com")
        print("not showing", result)
        # self.assertEquals(result.get(), 16)
        # self.assertTrue(result.successful())

    def test_stripe_create(self):
        response = stripe_controller.create_customer("data")
        self.assertEqual(response.object, "customer")

    def test_stripe_charge(self):
        response = stripe_controller.create_charge("description", "customer", "inr", 200)
        self.assertEqual(response, "Invalid customer request")

    def test_stripe_valid_charge(self):
        response = stripe_controller.create_charge("description", "cus_I7i3VCNQ7OEXhu", "inr", 200)
        self.assertEqual(response.object, "charge")

    def test_stripe_charge_card(self):
        response = stripe_controller.create_charge("description", "cus_I9SybE2dpqm9yg", "inr", 200)
        self.assertEqual(response, "Card not found")

    def test_stripe_source(self):
        response = stripe_controller.create_source("customer_stripeid")
        self.assertEqual(response, "Invalid customer id")

    def test_stripe_source_valid(self):
        response = stripe_controller.create_source("cus_I7i3VCNQ7OEXhu")
        self.assertEqual(response.funding, "credit")

    def test_stripe_retrieve(self):
        response = stripe_controller.retrieve_record("customer_stripeid")
        self.assertEqual(response, "Invalid customer id")

    def test_stripe_retrieve_valid(self):
        response = stripe_controller.retrieve_record("cus_I7i3VCNQ7OEXhu")
        self.assertEqual(response.object, "customer")
