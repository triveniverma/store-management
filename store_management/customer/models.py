'''
    models file for customer apis
'''
# django imports
from django.db import models
from product.models import Product
from coupons.models import Coupon
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.urls import reverse
from django_rest_passwordreset.signals import reset_password_token_created
from django.core.mail import send_mail 

# local imports
from store_management import constants

# Create your models here.
class CustomerRecord(User):
    """
        inheriting User and extending it
    """
    mobileno = models.IntegerField(unique=True)
    address = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    stripe_id = models.CharField(max_length=255, default='')
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        """
            changing name to Partner
        """
        verbose_name = "Customer Record"


    def __str__(self):
        return str(self.username)

class Cart(models.Model):
    """
        creating model for cart
    """
  
    customer_id = models.ForeignKey(CustomerRecord, on_delete=models.CASCADE)
    status = models.CharField(choices=constants.CART_STATUS, default='Inactive', max_length=20)
    total_amount = models.FloatField(default=0)
    coupon_code = models.ForeignKey(Coupon, on_delete=models.CASCADE, blank=True, null=True)
    discount_amount = models.FloatField(default=0)

    def __str__(self):
        return str(self.customer_id)



class ProductCart(models.Model):
    """
        cretaing product cart
    """
    product_id = models.ForeignKey(Product, on_delete=models.CASCADE)
    cart_id = models.ForeignKey(Cart, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)
    amount = models.FloatField(default=0)

    def save(self, *args, **kwargs):
        # print(int(self.amount))
        self.amount = (self.quantity)*(self.product_id.Price)
        super(ProductCart, self).save(*args, **kwargs)


class Transaction(models.Model):
    """
        transaction model to keep record of transactions
    """

    stripe_id = models.ForeignKey(CustomerRecord, on_delete=models.CASCADE)
    status = models.CharField(max_length=25, choices=constants.TRANSACTION_STATUS, 
                              default='Incomplete'
                             )
    mode = models.CharField(max_length=5, default=constants.PAYMENT)
    total_amount = models.FloatField()

class Orders(models.Model):
    """
        orders model to keep track of orders
    """
    transaction_id = models.ForeignKey(Transaction, on_delete=models.CASCADE)
    cart_id = models.ForeignKey(Cart, on_delete=models.CASCADE)
    address = models.CharField(max_length=255)
    status = models.CharField(max_length=30, choices=constants.ORDERS_STATUS, default='Placed')

@receiver(reset_password_token_created)
def password_reset_token_created(sender, instance, reset_password_token, *args, **kwargs):

    email_plaintext_message = "{}?token={}".format(reverse('password_reset:reset-password-request'), reset_password_token.key)

    send_mail(
        # title:
        "Password Reset for {title}".format(title="Some website title"),
        # message:
        email_plaintext_message,
        # from:
        "blogsfroma@gmail.com",
        # to:
        [reset_password_token.user.email]
    )

