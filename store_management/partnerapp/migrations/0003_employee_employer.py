# Generated by Django 3.1 on 2020-09-01 05:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('partnerapp', '0002_employee'),
    ]

    operations = [
        migrations.AddField(
            model_name='employee',
            name='Employer',
            field=models.ForeignKey(default=2, on_delete=django.db.models.deletion.CASCADE, to='partnerapp.partner'),
        ),
    ]
