'''
    python imports
'''
import datetime
'''
    django imports
'''
from django.shortcuts import render
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.views.generic import View, UpdateView, DeleteView, ListView, DetailView, TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.conf import settings
from django.core.mail import send_mail
# local imports
from partnerapp.forms import PartnerForm, EmployeeForm
from partnerapp.models import Partner,Employee
from payroll.models import Payroll
from attendance.models import EmpAttendance
from utils.controllers import payroll_controllers

class PartnerSignup(View):
    """
        partner signup view
    """
    def get(self, request):
        '''
            partner signup request
        '''
        partner_sign = PartnerForm
        return render(request, "partnerapp/partner.html", {'form':partner_sign})

    def post(self, request):
        '''
            adding partner through post request
        '''
        partner_form = PartnerForm(request.POST)
        if partner_form.is_valid():
            partner = partner_form.save()
            partner.set_password(partner.password)
            partner.is_staff = True
            # # import pdb; pdb.set_trace()
            permission = Permission.objects.get(name='Can view Employee')
            partner.user_permissions.add(permission)
            partner.save()
            return HttpResponseRedirect('/partner/signup/success')
        return render(request, "partnerapp/partner.html", {'form':partner_form})

class UpdatePartner(View, LoginRequiredMixin):
    """
         for updating the partner
    """
    model = Partner
    fields = ('username','password','email','first_name',
              'last_name','Mobileno','Address'
             )
    template_name = "partnerapp/partner.html"

    def get_success_url(self):
        return reverse('dashboard')


class EmployeeSignup(View, LoginRequiredMixin):
    """
        employee signup view
    """

    def send_email(self, user, user_email, user_pwd):# pragma: no cover
        '''
            sending email for login credentials
        '''
        subject = "Login Credentials"
        message = f'Hello {user} your credentials are {user} and {user_pwd}'
        recipient_list = [user_email]
        email_from = settings.EMAIL_HOST_USER
        send_mail(subject, message, email_from, recipient_list)


    def get(self, request):
        '''
            employee signup request
        '''
        employee_sign = EmployeeForm
        return render(request, "partnerapp/employee.html", {'form':employee_sign})

    def post(self, request):
        '''
            adding employee through post request
        '''
        employee_form = EmployeeForm(request.POST)
        # import pdb; pdb.set_trace()
        if employee_form.is_valid():
            employee = employee_form.save(commit=False)
            partnerid = Partner.objects.get(username=request.user)
            employee.Employer = partnerid
            password = employee.password
            employee.save()
            self.send_email(employee.username, employee.email, password )
            return HttpResponseRedirect('/partner/employee')
        return render(request, "partnerapp/employee.html", {'form':employee_form})

class ListEmployee(LoginRequiredMixin, ListView):
    """
        list of employees
    """
    model = Employee
    template_name = "partnerapp/employee_list.html"
    context_object_name = "employee_list"

    def get_queryset(self):
        ''' get employee of particular partner '''
        return Employee.objects.filter(Employer = self.request.user)

class DetailEmployee(LoginRequiredMixin, DetailView):
    """
        for detail of employee
    """
    model = Employee
    template_name = "partnerapp/employee_detail.html"
    context_object_name = "employee"

class UpdateEmployee(LoginRequiredMixin , UpdateView):
    """
        for updating the employee
    """
    model = Employee
    fields = ('email','first_name','last_name','Mobileno',
             'Address','Salary','Department','Blood_Group')
    template_name = "partnerapp/employee.html"

    def get_success_url(self):
        return reverse('emplist')

class DeleteEmployee(LoginRequiredMixin , DeleteView):
    """
        for updating the employee
    """
    model = Employee
    template_name = "partnerapp/employee_confirm_delete.html"

    def get_success_url(self):
        return reverse('emplist')


class Home(View):
    """
        to check if the user is employee or partner
    """
    def get(self, request):
        '''
            function to redirect either to partner or employee dashboard
        '''
        if Partner.objects.filter(username=request.user).exists():
            return HttpResponseRedirect('/partner/dashboard')
        elif Employee.objects.filter(username=request.user).exists():
            return HttpResponseRedirect('/partner/dashboard/employee')
        else:
            return HttpResponseRedirect('/partner/dashboard')


class Dashboard(TemplateView, LoginRequiredMixin):
    """
        to redirect to partner dashboard
    """
    template_name = "partnerapp/dashboard.html"


class EmpDashboard(TemplateView, LoginRequiredMixin):
    """
        to redirect to partner dashboard
    """
    template_name = "partnerapp/employeedash.html"

class SignupResponse(TemplateView):
    """
        to redirect to registration success page
    """
    template_name = "partnerapp/signup_response.html"

class GeneratePayroll(View, LoginRequiredMixin):
    '''
        to generate payroll for all employees of partner
    '''

    def get(self, request):
        '''
            function that is with the help of payorll controller
            generating payslips
        '''
        now_month = datetime.datetime.now().month
        now_year = datetime.datetime.now().year

        if now_month == 1:
            now_month = 13
            now_year -= 1

        working_days = payroll_controllers.number_working_days(now_year, now_month-1)
        all_emp = Employee.objects.filter(Employer=request.user)
        for pay_emp in all_emp:

            empid = Employee.objects.get(username=pay_emp)
            if not Payroll.objects.filter(employee_id=empid).exists():
                attend_sign = EmpAttendance.objects.filter(employeeid=empid,
                                                           time__month=now_month-1,
                                                           time_type='in time'
                                                          )
                attend_signout = EmpAttendance.objects.filter(employeeid=empid,
                                                              time__month=now_month-1,
                                                              time_type='out time'
                                                             )
                in_time, out_time, difference = ([] for initial_arr in range(3))
                full_day, half_day = 0, 0

                for hour_in in attend_sign:
                    in_time.append(hour_in.time)

                for hour_out in attend_signout:
                    out_time.append(hour_out.time)

                for list_in, list_out in zip(in_time, out_time):
                    difference.append(str(list_out - list_in))

                for time in difference:
                    split_time = int(time.split(":")[0])
                    if split_time >= 6:
                        full_day += 1
                    elif(split_time > 4 and split_time < 8):
                        half_day += 1

                total_salary = payroll_controllers.salary_calculation(empid.Salary,
                                                                      working_days,
                                                                      full_day, half_day
                                                                     )
                pay_save = Payroll(month=now_month-1, year=now_year,
                                   employee_id=empid, salary=empid.Salary,
                                   total=total_salary
                                  )
                pay_save.save()
        return HttpResponse("Receipts are generated")

class RedirectView(View):
    """
        will redirect to login page as first page
    """
    def get(self, request):
        return HttpResponseRedirect('/partner/accounts/login')