"""
    django imports
"""
from django.db import models
from django.contrib.auth.models import User
from store_management import constants

class Partner(User):
    """
        inheriting User and extending it
    """
    Mobileno = models.IntegerField(unique=True)
    Address = models.CharField(max_length=255)

    class Meta:
        """
            changing name to Partner
        """
        verbose_name = "Partner"

    def __str__(self):
        return str(self.username)

class Employee(User):
    """
        inheriting User and extending it for employee
    """
    Salary = models.IntegerField()
    Mobileno = models.IntegerField(unique=True)
    Address = models.CharField(max_length=255)
    Department = models.CharField(max_length=255)
    Blood_Group = models.CharField(max_length=10, choices=constants.BLOOD_GROUP, default='A+ve')
    Employer = models.ForeignKey(Partner, on_delete=models.CASCADE, default=2)

    class Meta:
        """
            changing name to Partner
        """
        verbose_name = "Employee"

    def save(self, *args, **kwargs):
        self.set_password(self.password)
        super(Employee, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.username)
