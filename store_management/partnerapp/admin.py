"""
    django imports
"""
from django.contrib import admin

# local imports
from partnerapp.models import Employee, Partner

# Register your models here.
class EmployeeAdmin(admin.ModelAdmin):
    '''
        will take to you to template which is responsible for generating fee receipt
    '''
    change_list_template = 'attendance/change_list.html'

admin.site.register(Partner)
admin.site.register(Employee, EmployeeAdmin)
