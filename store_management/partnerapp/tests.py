from django.test import TestCase, Client
from django.urls import reverse
from mock import patch
from partnerapp import views
from partnerapp.models import Partner, Employee
from payroll.models import Payroll
from partnerapp.forms import PartnerForm, EmployeeForm
from partnerapp.views import EmployeeSignup

def mock_send_email(self):
    print("mocked")

class TestView(TestCase):

    def setUp(self):
        
        self.client = Client()
        self.signup_url = reverse('signup')
        self.dashboard_url = reverse('dashboard')
        self.empdashboard_url = reverse('employee_dash')
        self.sign_emp = reverse('signup_employee')
        self.emp_list = reverse('emplist')

        self.partner = Partner.objects.create(
            password='123456',
            username='testuser1',
            first_name='lalita',
            last_name='agrawal',
            email='aaa@gmail.com',
            Mobileno='9898123455',
            Address='aaa123'
        )
        self.employee = Employee.objects.create(
            password='123456',
            username= 'testuser900',
            first_name='lalita',
            last_name='agrawal',
            email='aaa0000@gmail.com',
            Mobileno='9898123005',
            Address='aaa123',
            Salary='3400',
            Department='Python',
            Blood_Group='A+ve',
            Employer= Partner.objects.get(id=1)

        )
        print(Partner.objects.get(id=1))


    def test_partner_signup_get(self):

        response = self.client.get(self.signup_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "partnerapp/partner.html")

    def test_partner_dashboard(self):

        response = self.client.get(self.dashboard_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "partnerapp/dashboard.html")

    def test_employee_dashboard(self):

        response = self.client.get(self.empdashboard_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "partnerapp/employeedash.html")

    def test_employee_signup_get(self):

        response = self.client.get(self.sign_emp)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "partnerapp/employee.html")

    def test_partner_signup_post(self):

        response = self.client.post(self.signup_url, {

            'password':'123456',
            'username': 'testuser',
            'first_name':'lalita',
            'last_name':'agrawal',
            'email':'aaa@gmail.com',
            'Mobileno':'9898123455',
            'Address':'aaa123',
            
        })

        self.assertEquals(response.status_code, 200)
        print(response)
        # self.assertRedirects(response, '/partner/signup/success')

    @patch('partnerapp.views.EmployeeSignup.send_email', side_effect=mock_send_email)
    def test_employee_signup_post(self, *args, **kwargs):

        print("emp", Partner.objects.get(id=1))
        data = {

            'password':'123456',
            'username': 'testuser1',
            'first_name':'lalita',
            'last_name':'agrawal',
            'email':'aaa@gmail.com',
            'Mobileno':'9898123455',
            'Address':'aaa123',
            'Salary':'3400',
            'Department':'Python',
            'Blood_Group':'A+ve',
            'Employer': Partner.objects.get(id=1)
        }
        response = self.client.post(self.sign_emp, data)
        print("res partner", response)
        self.assertEquals(response.status_code, 200)

    def test_success_url(self):
        response = self.client.get(self.emp_list)
        self.assertEquals(response.status_code, 302)

    def test_employee_detail(self):
        response = self.client.get('/partner/employee/1/detail')
        self.assertEquals(response.status_code, 302)

    def test_employee_update(self):
        response = self.client.get('/partner/employee/1/update')
        self.assertEquals(response.status_code, 302)

    def test_employee_delete(self):
        response = self.client.get('/partner/employee/1/delete')
        self.assertEquals(response.status_code, 302)

    def test_change_password(self):
        response = self.client.post('/partner/change_password')
        self.assertEquals(response.status_code, 301)

    def test_generate_payroll(self):
        response = self.client.post('/partner/generate')
        self.assertEquals(response.status_code, 301)

    def test_invalid_form_partner(self):
        create_form = Partner.objects.create(
            password='123456',
            username='testuser2',
            first_name='lalita',
            last_name='agrawal',
            email='aaa90@gmail.com',
            Mobileno='98081',
            Address='aaa123'
            )
        data = {'password': create_form.password, 'username': create_form.username,
                'first_name': create_form.first_name, 'last_name': create_form.last_name,
                'email': create_form.email, 'Mobileno': create_form.Mobileno,
                'Address': create_form.Address}
        form = PartnerForm(data=data)
        self.assertFalse(form.is_valid())
        data = {'password': create_form.password, 'username': create_form.username,
                'first_name': create_form.first_name, 'last_name': create_form.last_name,
                'email': create_form.email, 'Mobileno': '999123567890',
                'Address': create_form.Address}
        form = PartnerForm(data=data)
        self.assertFalse(form.is_valid())

    def test_invalid_form_employee(self):
        create_form = Employee.objects.create(
            password='123456',
            username= 'testuser90',
            first_name='lalita',
            last_name='agrawal',
            email='aaa0000@gmail.com',
            Mobileno='9898123455',
            Address='aaa123',
            Salary='3400',
            Department='Python',
            Blood_Group='A+ve',
            Employer= Partner.objects.get(id=1)
            )
        data = {'password': create_form.password, 'username': 'hello90',
                'first_name': create_form.first_name, 'last_name': create_form.last_name,
                'email': create_form.email, 'Mobileno': '9991890',
                'Address': create_form.Address, 'Salary': create_form.Salary, 
                'Department': create_form.Department, 'Blood_Group': create_form.Blood_Group,
                'Employer': create_form.Employer}
        form = EmployeeForm(data=data)
        self.assertFalse(form.is_valid())
