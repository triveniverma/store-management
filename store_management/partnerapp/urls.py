"""
    django imports
"""
from django.urls import path,include
# local imports
from partnerapp import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('signup/', views.PartnerSignup.as_view(), name='signup'),
    path('signup/success', views.SignupResponse.as_view(), name='signup_success'),
    path('signup_employee/', views.EmployeeSignup.as_view(), name='signup_employee'),
    path('home/', views.Home.as_view(), name = 'home'),
    path('dashboard/', views.Dashboard.as_view(), name='dashboard'),
    path('dashboard/employee', views.EmpDashboard.as_view(), name='employee_dash'),
    path('accounts/', include('django.contrib.auth.urls'), name='accounts'),
    path('employee/', views.ListEmployee.as_view(),name='emplist'),
    path('employee/<pk>/detail', views.DetailEmployee.as_view(), name='emp_detail'),
    path('employee/<pk>/update', views.UpdateEmployee.as_view(), name='emp_update'),
    path('employee/<pk>/delete', views.DeleteEmployee.as_view(), name='emp_delete'),
    path('generate/', views.GeneratePayroll.as_view(), name='payroll'),
    path('partner/<pk>/update', views.UpdatePartner.as_view(), name='partner_update'),
    path(
        'change_password/',
        auth_views.PasswordChangeView.as_view(
            template_name='commons/change-password.html',
            success_url = '/partner/dashboard'
        ),
        name='change_password'
    ),
]
