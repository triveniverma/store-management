"""
    django imports
"""
from django import forms
# local imports
from partnerapp.models import Partner,Employee

class PartnerForm(forms.ModelForm):
    """
        Creating signup form for partner
    """
    class Meta:
        """
            model:Partner
        """
        model = Partner
        fields = ('username','password','email','first_name','last_name','Mobileno','Address')
        widgets = {'password':forms.PasswordInput()}

    def clean(self):
        """function to apply validation on form submission"""
        cleaned_data = super().clean()
        phone_number = cleaned_data.get("Mobileno")

        if len(str(phone_number)) < 10:
            raise forms.ValidationError(" Phone number can not be less than 10 digits")
        if len(str(phone_number)) > 10:
            raise forms.ValidationError(" Phone number can't be greater than 10 ")



class EmployeeForm(forms.ModelForm):
    """
        Creating signup form for employee
    """
    class Meta:
        """
            model:Employee
        """
        model = Employee
        fields = ('username','password','email','first_name','last_name',
                  'Mobileno','Address','Salary','Department','Blood_Group')
        widgets = {'password':forms.PasswordInput()}

    def clean(self):
        """function to apply validation on form submission"""
        cleaned_data = super().clean()
        phone_number = cleaned_data.get("Mobileno")

        if len(str(phone_number)) < 10:
            raise forms.ValidationError(" Phone number can not be less than 10 digits")
        # if phone_number > 10:
        #     raise forms.ValidationError(" Phone number should start from 9 ")
