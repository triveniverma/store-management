from django.test import TestCase, Client
from django.urls import reverse
from promotion.views import CreatePromotion
from promotion.models import Promotion
from partnerapp.models import Partner
from mock import Mock
class TestPromotion(TestCase):

    def setUp(self):

        self.client = Client()
        self.form = Mock()
        self.promotion_createurl = reverse('create_promotion')
        self.promotion_list = reverse('listpromotion')
        self.promotion = Promotion.objects.create(
            tagline='happy sunday',
            description='sale'
        )

        self.partner = Partner.objects.create(
            password='123456',
            username='testuser1',
            first_name='lalita',
            last_name='agrawal',
            email='aaa@gmail.com',
            Mobileno='9898123455',
            Address='aaa123'
        )
        
        self.partner.set_password('123456')
        self.partner.save()

        self.credentials = {
            'username': 'testuser1',
            'password': '123456'
            }

    def test_create_promotion(self):

        self.form = {
            'tagline':'happy saturday',
            'description':'sale'
        }
        response = self.client.post(self.promotion_createurl, {
            'tagline':'happy saturday',
            'description':'sale'
        })
        self.assertEquals(response.status_code, 302)

    def test_promotion_detail(self):

        response = self.client.get('/promotions/promotion/1/detail')
        self.assertEquals(response.status_code, 302)


    def test_promotion_update(self):

        response = self.client.get('/promotions/promotion/1/update')
        self.assertEquals(response.status_code, 302)


    def test_promotion_delete(self):

        response = self.client.get('/promotions/promotion/1/delete')
        self.assertEquals(response.status_code, 302)

    def test_promotion_list(self):

        response = self.client.get(self.promotion_list)
        self.assertEquals(response.status_code, 302)

    def test_list(self):

        # data = {
        #     'password':'123456',
        #     'username':'testuser1',
        # }
        # response_login = self.client.post('/partner/accounts/login', self.credentials)
        # print(response_login)
        logged_in = self.client.login(username='testuser1', password='123456')
        print(logged_in)
        response = self.client.get(reverse('listpromotion'))
        print(response)
        self.assertEqual(response.status_code, 200)
