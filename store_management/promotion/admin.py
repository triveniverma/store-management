''' django imports '''
from django.contrib import admin
# local imports
from promotion.models import Promotion
# Register your models here.
admin.site.register(Promotion)
 