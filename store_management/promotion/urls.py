''' django imports '''
from django.urls import path
# local imports
from promotion import views


urlpatterns = [
    path('promotion/', views.CreatePromotion.as_view(), name='create_promotion'),
    path('promotion_list/', views.ListPromotion.as_view(),name='listpromotion'),
    path('promotion/<pk>/detail', views.DetailPromotion.as_view(), name='detail_promotion'),
    path('promotion/<pk>/update', views.UpdatePromotion.as_view(), name='update_promotion'),
    path('promotion/<pk>/delete', views.DeletePromotion.as_view(), name='delete_promotion'),
]
