'''
    django imports
'''
from django.views.generic import CreateView,UpdateView,DeleteView,ListView,DetailView
# from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse

# local imports
from promotion.models import Promotion

# Create your views here.
class CreatePromotion(LoginRequiredMixin, CreateView):
    '''
        create view to create promotion
    '''
    model = Promotion
    fields = ('tagline','description','image','day')
    template_name = "promotion/promotion_form.html"

    def form_valid(self, form):
        ''' check form validing and appending values to the form '''
        if form.is_valid():
            checkform = form.save()
            checkform.created_by = str(self.request.user)
            checkform.modified_by = str(self.request.user)
            checkform.save()
            return super().form_valid(form)

    def get_success_url(self):
        return reverse('listpromotion')

class ListPromotion(LoginRequiredMixin, ListView):
    '''
        list view of promotion
    '''
    model = Promotion
    template_name = "promotion/promotion_list.html"
    context_object_name = "promotion_list"

    def get_queryset(self):
        ''' filter objects of list by current user '''
        return Promotion.objects.filter(created_by = self.request.user)

class DetailPromotion(LoginRequiredMixin, DetailView):
    '''
        display the detail of promotion
    '''
    model = Promotion

class UpdatePromotion(LoginRequiredMixin , UpdateView):
    '''
        update the details of promotion
    '''
    model = Promotion
    fields = ('tagline','description','image','day')

    def get_success_url(self):
        return reverse('listpromotion')

class DeletePromotion(LoginRequiredMixin , DeleteView):
    '''
        delete the promotion
    '''
    model = Promotion
    def get_success_url(self):
        return reverse('listpromotion')
