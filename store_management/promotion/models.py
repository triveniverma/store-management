''' django imports '''
from django.db import models
from store_management import constants

# Create your models here.
class Promotion(models.Model):
    ''' promotion model for creating promotions content '''
    tagline = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    image = models.FileField(upload_to='promotion_images/')
    day = models.CharField(max_length=20, choices=constants.DAYS_OF_WEEK, default='Monday')
    created_by = models.CharField(max_length=255,default='')
    modified_by = models.CharField(max_length=255,default='')
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):# pragma: no cover
        return str(self.tagline)
 