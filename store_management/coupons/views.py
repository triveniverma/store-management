'''
    django imports
'''
from django.views.generic import UpdateView, DeleteView, ListView, DetailView
from django.views.generic.edit import FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse

# local imports
from .forms import CouponForm
from .models import Coupon
# Create your views here.

class CreateCoupon(LoginRequiredMixin, FormView):
    '''
        create coupon from formview
    '''
    template_name = 'coupons/coupon_form.html'
    form_class = CouponForm
    success_url = '/coupons/coupon_list'

    def form_valid(self, form):
        ''' check form validity and append fields '''
        if form.is_valid():
            checkform = form.save()
            checkform.created_by = str(self.request.user)
            checkform.modified_by = str(self.request.user)
            checkform.save()
            return super().form_valid(form)


class ListCoupons(LoginRequiredMixin, ListView):
    '''
        list view to view list of coupons
    '''
    model = Coupon
    template_name = "coupons/coupon_list.html"
    context_object_name = "coupon_list"

    def get_queryset(self):
        ''' filter the queryset by current user '''
        return Coupon.objects.filter(created_by = self.request.user)

class DetailCoupons(LoginRequiredMixin, DetailView):
    '''
        to view details of coupon
    '''
    model = Coupon

class UpdateCoupons(LoginRequiredMixin , UpdateView):
    '''
        to update the coupons
    '''
    model = Coupon
    fields = ('code', 'coupon_value', 'coupon_description', 'start_date','expire_date')

    def get_success_url(self):
        return reverse('listcoupon')

class DeleteCoupons(LoginRequiredMixin , DeleteView):
    '''
        to delete the coupons
    '''
    model = Coupon
    def get_success_url(self):
        return reverse('listcoupon')
