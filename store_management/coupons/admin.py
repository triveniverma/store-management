'''
	django imports
'''
from django.contrib import admin
# local imports
from coupons.models import Coupon
# Register your models here.
admin.site.register(Coupon)
 