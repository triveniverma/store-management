'''
	django imports
'''
from django.urls import path
# local imports
from coupons import views

urlpatterns = [
    path('coupon/', views.CreateCoupon.as_view(), name='create_coupon'),
    path('coupon_list/', views.ListCoupons.as_view(),name='listcoupon'),
    path('coupon/<pk>/detail/', views.DetailCoupons.as_view(), name='detail_coupon'),
    path('coupon/<pk>/update/', views.UpdateCoupons.as_view(), name='update_coupon'),
    path('coupon/<pk>/delete/', views.DeleteCoupons.as_view(), name='delete_coupons'),
]
