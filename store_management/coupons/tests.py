from django.test import TestCase, Client
from django.urls import reverse
from coupons import views
from coupons.forms import CouponForm
from coupons.models import Coupon

class TestCoupon(TestCase):

    def setUp(self):

        self.client = Client()
        self.coupon_createurl = reverse('create_coupon')
        self.coupon_list = reverse('listcoupon')
        self.coupon = Coupon.objects.create(
            code='500',
            coupon_value=200,
            start_date='2020-09-30',
            expire_date='2020-10-5'
        )

    def test_create_coupon(self):

        response = self.client.post(self.coupon_createurl, {
            'code':'AB600',
            'coupon_value':400,
            'start_date':'2020-09-30',
            'expire_date':'2020-10-5',
        })
        self.assertEquals(response.status_code, 302)

    def test_coupon_detail(self):

        response = self.client.get('/coupons/coupon/1/detail')
        self.assertEquals(response.status_code, 301)


    def test_coupon_update(self):

        response = self.client.get('/coupons/coupon/1/update')
        self.assertEquals(response.status_code, 301)

    def test_coupon_delete(self):

        response = self.client.get('/coupons/coupon/1/delete')
        self.assertEquals(response.status_code, 301) 

    def test_valid_form(self):
        create_form = Coupon.objects.create(code='AB200',
                                            coupon_value=200,
                                            start_date='2020-11-4',
                                            expire_date='2020-11-25'
                                           )
        data = {'code': '300', 'coupon_value': create_form.coupon_value,
                'start_date': create_form.start_date, 'coupon_description': 'IN AMOUNT',
                'expire_date': create_form.expire_date}
        form = CouponForm(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        create_form = Coupon.objects.create(code='AB200',
                                            coupon_value=200,
                                            start_date='2020-11-4',
                                            expire_date='2020-10-25'
                                           )
        data = {'code': '300', 'coupon_value': create_form.coupon_value,
                'start_date': create_form.start_date, 'coupon_description': 'IN AMOUNT',
                'expire_date': create_form.expire_date}
        form = CouponForm(data=data)
        self.assertFalse(form.is_valid())

    def test_list_coupon(self):
        response = self.client.get(self.coupon_list)
        self.assertEquals(response.status_code, 302)
