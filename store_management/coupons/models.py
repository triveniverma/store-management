'''
    django imports
'''
from django.db import models
# local imports
from store_management import constants

# Create your models here.
class Coupon(models.Model):
    """
        coupon model fields
    """

    code = models.CharField(max_length=255, unique=True)
    coupon_value = models.IntegerField(default=0)
    coupon_description = models.CharField(max_length=15, choices=constants.COUPON_CHOICE,
                                         default='IN AMOUNT'
                                         )
    start_date = models.DateField()
    expire_date = models.DateField()
    created_by = models.CharField(max_length=255,default='')
    modified_by = models.CharField(max_length=255,default='')

    def __str__(self):# pragma: no cover
        ''' returns code '''
        return str(self.code)
