'''
    django imports
'''
from django import forms
# local imports
from coupons.models import Coupon

class DateInput(forms.DateInput):
    '''
        to get date input
    '''
    input_type = 'date'

class CouponForm(forms.ModelForm):
    '''
        coupon form with below fields and widgets
    '''
    class Meta:
        ''' meta class with models coupon and its fields '''
        model = Coupon
        fields = ('code', 'coupon_value', 'coupon_description', 'start_date','expire_date')
        widgets = {'start_date':DateInput(),
                   'expire_date':DateInput()}
    def clean(self):
        ''' to raise exception if expire date is less than start date '''
        cleaned_data = super().clean()
        start_date = cleaned_data.get("start_date")
        expire_date = cleaned_data.get("expire_date")
        if expire_date < start_date:
            raise forms.ValidationError("Expire date should be greater than Start date.")
            