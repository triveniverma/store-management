'''
    django imports
'''
from django.core.mail import send_mail
from django.conf import settings
from email.mime.multipart import MIMEMultipart
# from email.MIMEImage import MIMEImage
from email.mime.image import MIMEImage
from email.mime.text import MIMEText
# local imports
from customer.models import CustomerRecord
from promotion.models import Promotion
# from django.core.mail import EmailMessage
# import email.message
from django.template.loader import get_template
from django import template

from utils.controllers import send_mail

import os

def mail():# pragma: no cover
    '''
        sending latest promotion mail to all the customers through smtp
    '''
    get_promotion = Promotion.objects.latest('created_at')
    total_customer = CustomerRecord.objects.values_list('email', flat=True)

    email_from = settings.EMAIL_HOST_USER
    email_to = total_customer

    msg = MIMEMultipart('alternative')
    msg['Subject'] = get_promotion.tagline
    msg['From'] = email_from
    msg['To'] =  ", ".join(str(customer) for customer in email_to)

    image_url = get_promotion.image.url
    path = os.getenv("BASE_URL") + image_url

    # Create the body of the message (a plain-text and an HTML version).
    text = get_template('email/data.txt').render()
    html = get_template('email/data.html').render({'path':path})
   

    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')
    msg.attach(part1)
    msg.attach(part2)
    send_mail.mail_controller(email_from, email_to, msg)
    print("check4")
