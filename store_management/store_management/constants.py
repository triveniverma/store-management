CHOICE_TIME = (
                ('in time','IN TIME'),
                ('out time',('OUT TIME'))
              )

COUPON_CHOICE = (
                    ('IN AMOUNT', 'IN AMOUNT'),
                    ('IN PERCENTAGE', 'IN PERCENTAGE' )
                )

CART_STATUS = (
                ('Active', 'Active'),
                ('Inactive', 'Inactive'),
              )

TRANSACTION_STATUS = (
                        ('Incomplete', 'Incomplete'),
                        ('Payment Done', 'Payment Done'),
                     )
PAYMENT = "CC"

YEAR = 2020

MONTH_CHOICES = [(i,i) for i in range(1,13)]

DAYS_OF_WEEK = (
                    ('Monday', 'Monday'),
                    ('Tuesday', 'Tuesday'),
                    ('Wednesday', 'Wednesday'),
                    ('Thursday', 'Thursday'),
                    ('Friday', 'Friday'),
                    ('Saturday', 'Saturday'),
                    ('Sunday', 'Sunday'),
               )

ORDERS_STATUS = (
                    ('Placed', 'Placed'),
                    ('In progress', 'In progress'),

                )

BLOOD_GROUP =   (

                    ('A+ve', 'A+ve'),
                    ('A-ve', 'A-ve'),
                    ('B+ve', 'B+veve'),
                    ('B-ve', 'B-ve'),
                    ('AB+ve', 'AB+ve'),
                    ('AB-ve', 'AB-ve'),
                    ('O+ve', 'O+ve'),
                    ('O-ve', 'O-ve'),
                    ('Unknown', 'Unknown')
                )
