'''
    django imports
'''
from django.views.generic import CreateView,UpdateView,DeleteView,ListView,DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
# local imports
from product.models import Product

# Create your views here.

class CreateProduct(LoginRequiredMixin, CreateView):
    '''
        create product view
    '''
    model = Product
    fields = ('Name','Category','Price','Description','Image','Discount','Quantity')
    template_name = "product/product_form.html"

    def form_valid(self,form):
        '''
            check form validing and appending values to the form
        '''
        if form.is_valid():
            checkform = form.save()
            checkform.Created_By = str(self.request.user)
            checkform.Modified_By = str(self.request.user)
            checkform.save()
            return super().form_valid(form)

    def get_success_url(self):
        return reverse('listproduct')

class ListProduct(LoginRequiredMixin, ListView):
    '''
        list view of product
    '''
    model = Product
    template_name = "product/product_list.html"
    context_object_name = "product_list"

    def get_queryset(self):
        ''' filter objects of list by current user '''
        return Product.objects.filter(Created_By = self.request.user)

class DetailProduct(LoginRequiredMixin, DetailView):
    '''
        display the detail of product
    '''
    model = Product

class UpdateProduct(LoginRequiredMixin , UpdateView):
    '''
        update the details of product
    '''
    model = Product
    fields = ('Name','Category','Price','Description','Image','Discount','Quantity')

    def get_success_url(self):
        return reverse('listproduct')

class DeleteProduct(LoginRequiredMixin , DeleteView):
    '''
        delete the product
    '''
    model = Product
    def get_success_url(self):
        return reverse('listproduct')
