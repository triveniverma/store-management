"""
    django imports
"""
from django.db import models

# Create your models here.
class Product(models.Model):
    """
        creating model for product creation
    """
    Name = models.CharField(max_length=255)
    Category = models.CharField(max_length=255)
    Price = models.IntegerField()
    Description = models.CharField(max_length=255)
    Image = models.FileField(upload_to='product_images/')
    Discount = models.IntegerField(default=0)
    Quantity = models.IntegerField()
    Created_At = models.DateField(auto_now_add=True)
    Created_By = models.CharField(max_length=255)
    Modified_At = models.DateField(auto_now=True)
    Modified_By = models.CharField(max_length=255, blank=True, default='')

    def __str__(self):
        return str(self.Name)
        