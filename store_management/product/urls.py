"""
    django imports
"""
from django.urls import path

# local imports
from product import views


urlpatterns = [
    path('product/', views.CreateProduct.as_view(), name='product'),
    path('product_list/', views.ListProduct.as_view(),name='listproduct'),
    path('product/<pk>/detail', views.DetailProduct.as_view(), name='product_detail'),
    path('product/<pk>/update', views.UpdateProduct.as_view(), name='product_update'),
    path('product/<pk>/delete', views.DeleteProduct.as_view(), name='product_delete'),
]
