"""
    django imports
"""
from django.contrib import admin
# local imports
from product.models import Product
# Register your models here.
admin.site.register(Product)
 