from django.test import TestCase, Client
from django.urls import reverse
from product import views
from product.models import Product
from partnerapp.models import Partner
# Create your tests here.
class TestProduct(TestCase):

    def setUp(self):

        self.client = Client()
        self.product_createurl = reverse('product')
        self.productlist_url = reverse('listproduct')
        self.product = Product.objects.create(
            Name='Kitkat',
            Category='Chocolate',
            Price=30,
            Description='Tasty',
            Quantity=10,
            Created_By='Janvi'
        )
        self.partner = Partner.objects.create(
            password='123456',
            username='testuser1',
            first_name='lalita',
            last_name='agrawal',
            email='aaa@gmail.com',
            Mobileno='9898123455',
            Address='aaa123'
        )
        
        self.partner.set_password('123456')
        self.partner.save()

        self.credentials = {
            'username': 'testuser1',
            'password': '123456'
            }

    def test_create_product(self):

        response = self.client.post(self.product_createurl, {
            'Name':'Perk',
            'Category':'Chocolate',
            'Price':30,
            'Description':'Tasty',
            'Quantity':10,
            'Created_By':'Janvi'
        })
        self.assertEquals(response.status_code, 302)

    def test_product_form(self):

        logged_in = self.client.login(username='testuser1', password='123456')
        response = self.client.post(self.product_createurl, {
            'Name':'Dark Chocolate',
            'Category':'Chocolate',
            'Price':30,
            'Description':'Tasty',
            'Quantity':10,
            'Created_By':'testuser1',
            'Modified_By':'testuser1'
        })
        self.assertEquals(response.status_code, 200)

    def test_product_list(self):

        logged_in = self.client.login(username='testuser1', password='123456')
        response = self.client.get('/products/product_list/')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'product/product_list.html')

    def test_product_detail(self):

        response = self.client.get('/products/product/1/detail')
        self.assertEquals(response.status_code, 302)

    def test_product_update(self):

        response = self.client.get('/products/product/1/update')
        self.assertEquals(response.status_code, 302)

    def test_product_delete(self):

        response = self.client.get('/products/product/1/delete')
        self.assertEquals(response.status_code, 302)          


