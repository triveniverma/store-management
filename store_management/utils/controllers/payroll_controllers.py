'''
    python imports
'''
import decimal
import calendar
from calendar import monthrange, monthcalendar, month

def number_of_days(year, month):
    '''
        to count number of days in a month
    '''
    try:
        return monthrange(year, month)[1]
    except calendar.IllegalMonthError as e:
        return "Not valid month"

def number_working_days(year, month):
    '''
        to count number of working days in a month
    '''
    try:
        working_days = 0
        for each_week in monthcalendar(year, month):
            if each_week[0] != 0:
                working_days+=1
            if each_week[1] != 0:
                working_days+=1
            if each_week[2] != 0:
                working_days+=1
            if each_week[3] != 0:
                working_days+=1
            if each_week[4] != 0:
                working_days+=1
        return working_days
    except calendar.IllegalMonthError as e:
        return "Not valid month"

def salary_calculation(salary, work_days, full_day, half_day):
    '''
        to calculate salary of employee
    '''
    try:
        per_day_salary = decimal.Decimal(salary)/decimal.Decimal(work_days)
        half_day_salary = per_day_salary/2
        total_salary = (full_day)*per_day_salary + half_day*half_day_salary
        return total_salary
    except ZeroDivisionError as e:
        return "Can't divide by zero"