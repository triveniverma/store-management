'''
    third party module
'''
import smtplib
# django imports
from django.conf import settings


def mail_controller(email_from, email_to, msg):
    '''
        controller for sending an email
    '''
    smtp = smtplib.SMTP(settings.EMAIL_HOST, settings.EMAIL_USE_TLS)
    smtp.starttls()
    smtp.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
    print(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
    smtp.sendmail(email_from, email_to, msg.as_string())
    smtp.quit()

