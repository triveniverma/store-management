'''
    third party module
'''
import stripe
import os
stripe.api_key = os.getenv("API_KEY")

def create_customer(data):
    '''
        stripe will create customer
    '''
    customer = stripe.Customer.create(
            description=data  
            )
    return customer

def create_charge(description, customer, currency, amount):
    '''
        stripe will generate the charge
    '''
    try:
        charge = stripe.Charge.create(
                                description=description,
                                customer=customer,
                                currency=currency,
                                amount=amount,
                            )
        return charge
    except stripe.error.CardError as e:
        return "Card not found"
    except stripe.error.InvalidRequestError as e:
        return "Invalid customer request"

def create_source(customer_stripeid):
    '''
        will create test source for the customer
    '''
    try:
        source = stripe.Customer.create_source(
              customer_stripeid,
              source="tok_visa",
            )
        return source
    except stripe.error.InvalidRequestError as e:
        return "Invalid customer id"

def retrieve_record(customer_stripeid):
    '''
        will retrieve the record for the customer_id
    '''
    try:
        record = stripe.Customer.retrieve(customer_stripeid)
        return record
    except stripe.error.InvalidRequestError as e:
        return "Invalid customer id"
