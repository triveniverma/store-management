''' django imports '''
from django.db import models

# local imports
from partnerapp.models import Employee
from store_management import constants

# Create your models here.
# class Attendance(models.Model):

#     CHOICE_TIME = (('in time','IN TIME'),('out time',('OUT TIME')))
#     employeeid = models.ForeignKey(Employee, on_delete=models.CASCADE)
#     time_type = models.CharField(max_length=20, choices=CHOICE_TIME, default='in time')
#     date = models.DateField(auto_now_add=True)
#     time = models.TimeField(auto_now_add=True)

class EmpAttendance(models.Model):
    '''
        creating model for employee attendance
    '''
    employeeid = models.ForeignKey(Employee, on_delete=models.CASCADE)
    time_type = models.CharField(max_length=20, choices=constants.CHOICE_TIME, default='in time')
    time = models.DateTimeField(auto_now_add=True)
 