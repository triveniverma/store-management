'''
    django imports
'''
from django.contrib import admin
# local imports
from .models import EmpAttendance

# Register your models here.

class EmpAttendanceAdmin(admin.ModelAdmin):
    '''
        displaying list for employee attendance model
    '''
    list_display = ['employeeid', 'time_type', 'time']


admin.site.register(EmpAttendance, EmpAttendanceAdmin)
