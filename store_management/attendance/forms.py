'''
    django imports
'''
from django import forms
# local imports
from attendance.models import EmpAttendance

class EmpAttendanceForm(forms.ModelForm):
    '''
        making form for employee attendance
    '''
    class Meta:
        '''
            using all the fields with empattendance model
        '''
        model = EmpAttendance
        fields = '__all__'
   