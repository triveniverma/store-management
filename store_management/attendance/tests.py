from django.test import TestCase, Client
from django.urls import reverse
from attendance import views
from attendance.models import EmpAttendance
from attendance.forms import EmpAttendanceForm
from partnerapp.models import Employee, Partner

class TestAttendance(TestCase):

    def setUp(self):
        self.client = Client()
        self.signin_url = reverse('signin')
        self.attendance = reverse('listattendance')

        self.partner = Partner.objects.create(
            password='123456',
            username='testuser1',
            first_name='lalita',
            last_name='agrawal',
            email='aaa@gmail.com',
            Mobileno='9898123455',
            Address='aaa123'
        )
        self.employee = Employee.objects.create(
            password='123456',
            username= 'testuser2',
            first_name='lalita',
            last_name='agrawal',
            email='aaa@gmail.com',
            Mobileno='9898123455',
            Address='aaa123',
            Salary='3400',
            Department='Python',
            Blood_Group='A+ve',
            Employer=Partner.objects.get(id=1)
        )
        self.employee.set_password('123456')
        self.employee.save()

        self.credentials = {
            'username': 'testuser2',
            'password': '123456'
            }

    def test_update_emp(self):
        data = {
            'password':'123456',
            'username':'testuser2',
            'first_name':'lalita123',
            'last_name':'agrawal',
            'email':'aaa@gmail.com',
            'Mobileno':'9898123455',
            'Address':'aaa123',
            'Salary':'3400',
            'Department':'Python',
            'Blood_Group':'A+ve',
            'Employer':Partner.objects.get(id=1)   
        }
        response = self.client.put('/attendance/employee/1', data, format='json')
        self.assertEqual(response.status_code, 302)

    def test_valid_form(self):
        create_form = EmpAttendance.objects.create(employeeid=Employee.objects.get(username='testuser2'))
        data = {'employeeid': create_form.employeeid, 'time_type': 'in time'}
        form = EmpAttendanceForm(data=data)
        print(form)
        self.assertTrue(form.is_valid())

    def test_signin(self):
        logged_in = self.client.login(username='testuser2', password='123456')
        response = self.client.get('/attendance/signin')
        self.assertEquals(response.status_code, 301)
        # self.assertTemplateUsed(response, "attendance/markattendance.html")

    def test_post_attend(self):
        logged_in = self.client.login(username='testuser2', password='123456')
        data = {
                    'employeeid': '1',
                    'time_type': 'in_time'
        }
        response = self.client.post('/attendance/signin', data)
        # print(response)
        self.assertEquals(response.status_code, 301)

    def test_sign_out(self):
        logged_in = self.client.login(username='testuser2', password='123456')
        response = self.client.get('/attendance/signout')
        self.assertEquals(response.status_code, 301)