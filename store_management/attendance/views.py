'''
    views for attendance app
'''
# python imports
from datetime import date

# django imports
from django.urls import reverse
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic import View, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin

# local imports
from attendance.models import EmpAttendance
from partnerapp.models import Employee


# Create your views here.

class SignIn(View, LoginRequiredMixin):
    """
        will record first signin time and redirect to signout page
    """

    def get(self, request):
        '''
            will render the signup page
        '''
        empid = Employee.objects.get(username=request.user)
        if EmpAttendance.objects.filter(employeeid=empid, time_type='in time', time__date=date.today()).exists():
            return HttpResponseRedirect("/attendance/signout")
        return render(request, "attendance/markattendance.html")

    def post(self, request):
        '''
            will mark the attendance
        '''
        # import pdb; pdb.set_trace()
        empid = Employee.objects.get(username=request.user)
        attend = EmpAttendance(employeeid=empid, time_type='in time')
        attend.save()
        return HttpResponseRedirect("/attendance/signout")

class SignOut(View, LoginRequiredMixin):
    """
        outattendance for signout
    """
    def get(self, request):
        """
           will render the signout page
        """
        return render(request, "attendance/outattendance.html")


    def post(self, request):
        """
            will record the last signout time
        """
        empid = Employee.objects.get(username=request.user)
        attend = EmpAttendance(employeeid=empid, time_type='out time')
        attend.save()
        return HttpResponseRedirect("/attendance/day_summary")


class WorkSummary(View, LoginRequiredMixin):
    """
        will give you current date summary
    """

    def delete_objects(self, del_obj):
        '''
            for deleting the unnecessary objects
        '''
        for obj_del in del_obj:
            obj_del.delete()


    def get(self, request):
        '''
            for summary
        '''
        today = date.today()
        today_in =  EmpAttendance.objects.filter(time__date=today,
                                                   employeeid=self.request.user,
                                                   time_type='in time')
        today_out =  EmpAttendance.objects.filter(time__date=today,
                                                  employeeid=self.request.user,
                                                  time_type='out time')

        del_in = today_in[1:len(today_in)]
        del_out = today_out[:len(today_out)-1]
        self.delete_objects(del_in)
        self.delete_objects(del_out)
        in_time = today_in[:1]
        out_time = EmpAttendance.objects.filter(employeeid=self.request.user,
                                                time_type='out time').order_by('-time')[0]
        data = {'in_time':in_time[0], 'out_time':out_time}
        return render(request, "attendance/worksummary.html",context=data)

class ListAttendance(View, LoginRequiredMixin):
    '''
        inheriting view to display attendance
    '''
    def get(self, request):
        '''
            will give you 10 recent swipes of attendance
        '''
        obj_in = EmpAttendance.objects.filter(employeeid=self.request.user,
                                              time_type='in time').order_by('-time')[:10]
        obj_out = EmpAttendance.objects.filter(employeeid=self.request.user,
                                               time_type='out time').order_by('-time')[:10]
        return render(request, "attendance/attendance_list.html",
                      {'obj_in':obj_in, 'obj_out':obj_out})


class UpdateEmployee(LoginRequiredMixin , UpdateView):
    '''
        inheriting update view to update the employee
    '''
    model = Employee
    fields = ('email','first_name','last_name','Mobileno','Address','Department','Blood_Group')
    template_name = "attendance/employee.html"

    def get_success_url(self):
        return reverse('listattendance')

# class PasswordChangeView(View, LoginRequiredMixin):
#     '''
#         changing password for the employee
#     '''
#     template_name = 'commons/change-password.html'
#     success_url = '/attendance/signin'

