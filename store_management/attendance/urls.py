'''
	django imports
'''
from django.urls import path
# local imports
from attendance import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('signin/', views.SignIn.as_view(), name='signin'),
    path('signout/', views.SignOut.as_view(), name='signout'),
    path('day_summary/', views.WorkSummary.as_view(), name='day_summary'),
    path('employee/', views.ListAttendance.as_view(), name='listattendance'),
    path('employee/<pk>', views.UpdateEmployee.as_view(), name='update_employee'),
    path(
        'change_password/',
        auth_views.PasswordChangeView.as_view(
            template_name='commons/change-password.html',
            success_url = '/attendance/signin'
        ),
        name='change_password'
    ),
]
