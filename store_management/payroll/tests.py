from django.test import TestCase
from utils.controllers import payroll_controllers
# Create your tests here.
class PayrollTestcase(TestCase):

    def test_number_of_days(self):
        response = payroll_controllers.number_of_days(2020, 9)
        self.assertEqual(response, 30)

    def test_invalid_days(self):
        response = payroll_controllers.number_of_days(2020, 0)
        self.assertEqual(response, "Not valid month")

    def test_number_of_working_days(self):
        response = payroll_controllers.number_working_days(2020, 9)
        self.assertEqual(response, 22)

    def test_invalid_work_days(self):
        response = payroll_controllers.number_working_days(2020, 0)
        self.assertEqual(response, "Not valid month")

    def test_payroll(self):
        response = payroll_controllers.salary_calculation(50000, 0, 20, 12)
        self.assertEqual(response, "Can't divide by zero")
        response = payroll_controllers.salary_calculation(20000, 20, 15, 0)
        self.assertEqual(response, 15000)
