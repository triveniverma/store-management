'''
    django imports
'''
from django.db import models
# local imports
from partnerapp.models import Employee
from store_management import constants
# global variable

# Create your models here.
class Payroll(models.Model):
    '''
        model for payroll generation
    '''
    month = models.IntegerField(choices=constants.MONTH_CHOICES)
    year = models.IntegerField(default=constants.YEAR)
    employee_id = models.ForeignKey(Employee, on_delete=models.CASCADE)
    salary = models.FloatField()
    bonus = models.IntegerField(default=0)
    total = models.FloatField()
