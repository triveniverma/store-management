'''
	django imports
'''
from django.contrib import admin
# local imports
from payroll.models import Payroll

# Register your models here.
admin.site.register(Payroll)
